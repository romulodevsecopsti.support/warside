package com.warsideband.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.warsideband.models.Contato;
import com.warsideband.models.Eventos;
import com.warsideband.repository.ContatoRepository;
import com.warsideband.repository.EventoRepository;

@Controller
public class MusicaController {
	
	@Autowired
	private ContatoRepository cr;
	
	@Autowired
	private EventoRepository er;
	
	@RequestMapping(value="/enviarContato", method=RequestMethod.GET)
	public String contato() {
		return "banda/formContato";
	}
	
	@RequestMapping(value="/enviarContato", method=RequestMethod.POST)
	public String contato(Contato contato) {
		
		cr.save(contato);
		
		return "redirect:/enviarContato";
	}
	
	@RequestMapping("/admin")
	public ModelAndView listaContatos() {
		ModelAndView mv = new ModelAndView("banda/lista");
		Iterable<Contato> contatos = cr.findAll();
		mv.addObject("contatos", contatos);
		return mv;
	}

	
	@RequestMapping("/deletarContato")
	public String deletarContato(long codigo){
		Contato contato = cr.findByCodigo(codigo);
		cr.delete(contato);
		return "redirect:/admin";
	}

	@RequestMapping(value="/enviarEvento", method=RequestMethod.GET)
	public String eventos() {
		return "banda/formEvento";
	}
	
	@RequestMapping(value="/enviarEvento", method=RequestMethod.POST)
	public String evento(Eventos eventos) {
		
		er.save(eventos);
		
		return "redirect:/enviarEvento";
	}
	
	@RequestMapping("/eventos")
	public ModelAndView listaEventos() {
		ModelAndView mv = new ModelAndView("banda/eventos");
		Iterable<Eventos> eventos = er.findAll();
		mv.addObject("eventos", eventos);
		return mv;
	}
	
	@RequestMapping("/admEventos")
	public ModelAndView admEventos() {
		ModelAndView mv = new ModelAndView("banda/admEventos");
		Iterable<Eventos> eventos = er.findAll();
		mv.addObject("eventos", eventos);
		return mv;
	}
	
	
	@RequestMapping("/deletarEvento")
	public String deletarEvento(long codigo){
		Eventos eventos = er.findByCodigo(codigo);
		er.delete(eventos);
		return "redirect:/admEventos";
	}	
	
	@RequestMapping("/musicas")
	public String musica() {
		return "banda/musica";
	}
	
	
	@RequestMapping("/galeria")
	public String galeria() {
		return "banda/galeria";
	}
	


	

}
